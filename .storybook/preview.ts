import { moduleMetadata, type Preview } from '@storybook/angular';
import { setCompodocJson } from '@storybook/addon-docs/angular';
import docJson from '../documentation.json';
import { INITIAL_VIEWPORTS } from '@storybook/addon-viewport';
import { HttpClientModule } from '@angular/common/http';
import { Router, RouterModule } from '@angular/router';
import { ENVIRONMENT_INITIALIZER, inject } from '@angular/core';

setCompodocJson(docJson);

// const globalModuleImports = moduleMetadata({
//   imports: [RouterModule, HttpClientModule],
//   providers: [Router],
// });

// const setRoutesMetadata = (fn: any, c: any) => {
//   const story = fn();
//   story.moduleMetadata.providers.push({
//     provide: ENVIRONMENT_INITIALIZER,
//     multi: true,
//     useValue() {
//       inject(Router).resetConfig(c.parameters?.routes || []);
//     },
//   });
//   return story;
// };

// export const decorators = [globalModuleImports, setRoutesMetadata];

const preview: Preview = {
  parameters: {
    layout: 'padded',
    docs: {
      story: {
        inline: false,
        iframeHeight: 700,
      },
    },
    viewport: {
      viewports: INITIAL_VIEWPORTS, // newViewports would be an ViewportMap. (see below for examples)
      defaultViewport: 'someDefault',
    },
    actions: { argTypesRegex: '^on[A-Z].*' },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/,
      },
    },
  },
};

export default preview;
