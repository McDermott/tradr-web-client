# ALPHA FEATURES LIST (PHASE 1)

## PAGES

## User Management

- [x] User Registration endpoint.
- [x] User Login endpoint.
- [x] Auth key validator endpoint
- [x] Delete user endpoint.
- [x] Update user details endpoint.
- [x] Get user images endpoint.

## Store Management

- [x] Create new store endpoint.
- [x] Add item to existing store endpoint.
- [x] Delete existing store endpoint.
- [x] Delete existing item endpoint.
- [x] Update store endpoint.
- [x] Get store items images endpoint.
- [x] Get store images endpoint.

## Store View

- [x] View Store endpoint.
<!-- -   [ ] (Optional) View individual item endpoint. --> // NOT NECESSARY AT PRESENT.

## Search

- [x] Summarised list endpoint.
- [x] Search endpoint.

## Moderation

- [x] Admin disable OR suspend store endpoint.
<!-- -   [ ] Admin disable user endpoint. --> // MOVED TO PHASE 2.
- [x] User report store endpoint.
- [x] Admin list of stores to be reviewed endpoint.
- [x] Create cronjob to clear suspensions.

## OUTSTANDING TASKS

- [x] Remove unused fields from store and user endpoints.
- [x] Implement language filter in Store Name and User Name, and Item Description.
- [x] Explore postcode based search.
- [x] Replace username and storename with @ in the beginning.
