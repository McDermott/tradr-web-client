import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LandingPageComponent } from '../pages/landing-page/landing-page.component';
import { landingPageResolver } from '../pages/landing-page/landing-page.resolver';
import { LoginPageComponent } from '../pages/login-page/login-page.component';
import { loginPageResolver } from '../pages/login-page/login-page.resolver';
import { HomePageComponent } from '../pages/home-page/home-page.component';
import { homePageResolver } from '../pages/home-page/home-page.resolver';
import { SearchPageComponent } from '../pages/search-page/search-page.component';
import { searchPageResolver } from '../pages/search-page/search-page.resolver';

export const routes: Routes = [
  { path: '', redirectTo: '/landing', pathMatch: 'full' },
  {
    path: 'home',
    component: HomePageComponent,
    resolve: { data: homePageResolver },
  },
  {
    path: 'login',
    component: LoginPageComponent,
    resolve: { data: loginPageResolver },
  },
  {
    path: 'landing',
    component: LandingPageComponent,
    resolve: { data: landingPageResolver },
  },
  {
    path: 'search',
    component: SearchPageComponent,
    resolve: {
      data: searchPageResolver,
    },
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class RoutingModule {}
