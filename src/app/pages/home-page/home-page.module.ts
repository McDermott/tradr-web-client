import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReuseableModule } from 'src/app/components/reuseable/reuseable.module';
import { HomePageComponent } from './home-page.component';

@NgModule({
  declarations: [HomePageComponent],
  imports: [CommonModule, ReuseableModule],
})
export class HomePageModule {}
