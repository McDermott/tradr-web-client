import { Readonly } from './../../components/reuseable/input/input.stories';
import { Component, inject } from '@angular/core';
import { IHomePage } from './home-page.resolver';
import { ActivatedRoute } from '@angular/router';
import { IStoreSummaryResponse } from 'src/app/interfaces/store-interfaces/IStoreSummaryResponse';
import { IStoreSummary } from 'src/app/interfaces/store-interfaces/IStoreSummary';
import { IResourceManagementService } from 'src/app/services/resource-management-service/resource-management.service';
import { StoreManagementService } from 'src/app/services/store-management-service/store-management-service.service';
import { MessageTransporterService } from 'src/app/services/message-transporter/message-transporter.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
})
export class HomePageComponent {
  public pageData?: IHomePage;
  public mostRecentStores: IStoreSummary[] = [];

  private readonly _activatedRoute: ActivatedRoute = inject(ActivatedRoute);
  private readonly _storeManagementService: StoreManagementService = inject(
    StoreManagementService
  );
  private readonly _messageTransporterService: MessageTransporterService =
    inject(MessageTransporterService);

  constructor() {
    this._activatedRoute.data.subscribe((pageDataFromResolver) => {
      this.pageData = pageDataFromResolver['data'] as IHomePage;
    });

    this._storeManagementService
      .getMostRecentStoresForHomePageDisplay()
      .subscribe((storeSummaryResponse: IStoreSummaryResponse) => {
        this._messageTransporterService.httpMessageInject.set(
          storeSummaryResponse.storeSummaryQueryStatus
        );
        this.mostRecentStores = storeSummaryResponse.stores ?? [];
      });
  }
}
