import { of } from 'rxjs';
import type { Meta, StoryObj } from '@storybook/angular';
import { ActivatedRoute, RouterLinkWithHref } from '@angular/router';
import { componentWrapperDecorator, moduleMetadata } from '@storybook/angular';
import { AppComponent } from '../app-page/app.component';
import { HomePageComponent } from './home-page.component';
import { homePageResolver } from './home-page.resolver';
import { StoreSummaryComponent } from 'src/app/components/reuseable/store-summary/store-summary.component';
import { ButtonComponent } from 'src/app/components/reuseable/button/button.component';

const meta: Meta<HomePageComponent> = {
  title: 'Home Page Component',
  component: HomePageComponent,
  excludeStories: /.*Data$/,
  parameters: {
    routes: [
      {
        path: 'login',
        resolver: homePageResolver,
      },
    ],
  },
  decorators: [
    componentWrapperDecorator(AppComponent),
    moduleMetadata({
      imports: [RouterLinkWithHref],
      declarations: [AppComponent, StoreSummaryComponent, ButtonComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({
              data: {
                pageStrings: {},
              },
            }),
          },
        },
      ],
    }),
  ],
  tags: ['autodocs'],
  render: (args: HomePageComponent) => ({
    props: {
      ...args,
    },
  }),
};

export default meta;

type Story = StoryObj<HomePageComponent>;

export const Default: Story = {
  args: {},
};
