import { inject } from '@angular/core';
import {
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
  ResolveFn,
} from '@angular/router';
import { UserManagementService } from 'src/app/services/user-management-service/user-management.service';

export interface IHomePage {
  pageStrings: {};
}

export const homePageResolver: ResolveFn<Partial<IHomePage>> = (
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot
) => {
  const userManagementService: UserManagementService = inject(
    UserManagementService
  );

  userManagementService.doUserCheck();

  return {
    pageStrings: {},
  };
};
