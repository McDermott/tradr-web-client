import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LandingPageComponent } from './landing-page.component';
import { ReuseableModule } from 'src/app/components/reuseable/reuseable.module';

@NgModule({
  declarations: [LandingPageComponent],
  imports: [CommonModule, ReuseableModule],
})
export class LandingPageModule {}
