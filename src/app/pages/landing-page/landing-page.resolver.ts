import { inject } from '@angular/core';
import {
  Router,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
  ResolveFn,
} from '@angular/router';
import { UserManagementService } from 'src/app/services/user-management-service/user-management.service';

export const landingPageResolver: ResolveFn<any> = (
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot
) => {
  const userManagementService: UserManagementService = inject(
    UserManagementService
  );
  userManagementService.doUserCheck();
  return {};
};
