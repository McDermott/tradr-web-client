import { inject } from '@angular/core';
import {
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
  ResolveFn,
} from '@angular/router';
import { UserManagementService } from 'src/app/services/user-management-service/user-management.service';

export interface ISearchPage {
  pageStrings: {};
}

export const searchPageResolver: ResolveFn<Partial<ISearchPage>> = (
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot
) => {
  const userManagementService: UserManagementService = inject(
    UserManagementService
  );

  userManagementService.doUserCheck();

  return {
    pageStrings: {},
  };
};
