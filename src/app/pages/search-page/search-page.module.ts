import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReuseableModule } from 'src/app/components/reuseable/reuseable.module';
import { SearchPageComponent } from './search-page.component';

@NgModule({
  declarations: [SearchPageComponent],
  imports: [CommonModule, ReuseableModule],
})
export class SearchPageModule {}
