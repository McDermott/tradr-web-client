import { of } from 'rxjs';
import type { Meta, StoryObj } from '@storybook/angular';
import { LoginPageComponent } from './login-page.component';
import { ActivatedRoute, RouterLinkWithHref } from '@angular/router';
import { loginPageResolver } from './login-page.resolver';
import { componentWrapperDecorator, moduleMetadata } from '@storybook/angular';
import { AppComponent } from '../app-page/app.component';
import { ReuseableModule } from 'src/app/components/reuseable/reuseable.module';

const meta: Meta<LoginPageComponent> = {
  title: 'Login Page Component',
  component: LoginPageComponent,
  excludeStories: /.*Data$/,
  parameters: {
    routes: [
      {
        path: 'login',
        resolver: loginPageResolver,
      },
    ],
  },
  decorators: [
    componentWrapperDecorator(AppComponent),
    moduleMetadata({
      imports: [ReuseableModule, RouterLinkWithHref],
      declarations: [AppComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({
              data: {
                pageStrings: {
                  siteName: 'TRADR',
                  siteSlogan: 'Your Digital Market Stall!',
                  yourEmail: 'Your Email',
                  emailPlaceholder: 'i.e. your.name@gmail.com',
                  emailWrong: 'Your Email is not valid',
                  yourPassword: 'Your Password',
                  passwordWrong: 'Your password is wrong.',
                  passwordPlaceholder: 'password',
                  logIn: 'Log in!',
                  dontHaveAnAccount: 'Create account.',
                },
              },
            }),
          },
        },
      ],
    }),
  ],
  tags: ['autodocs'],
  render: (args: LoginPageComponent) => ({
    props: {
      ...args,
    },
  }),
};

export default meta;

type Story = StoryObj<LoginPageComponent>;

export const Default: Story = {
  args: {},
};
