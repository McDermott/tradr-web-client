import { CommonModule } from '@angular/common';
import { Component, inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ILoginPage } from './login-page.resolver';
import { IUserLoginDetailsInsert } from 'src/app/interfaces/user-interfaces/IUserLoginDetailsInsert';
import {
  IPublicUserDetailsResponse,
  UserLoginEnum,
} from 'src/app/interfaces/user-interfaces/IPublicUserDetailsResponse';
import {
  IUserManagementService,
  UserManagementService,
} from 'src/app/services/user-management-service/user-management.service';
import { MessageTransporterService } from 'src/app/services/message-transporter/message-transporter.service';

@Component({
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
})
export class LoginPageComponent {
  public pageData?: ILoginPage;
  public loginData: IUserLoginDetailsInsert = {
    email: '',
    password: '',
  };
  public emailBad = false;
  public passwordBad = false;

  private readonly _activatedRoute: ActivatedRoute = inject(ActivatedRoute);
  private readonly _router: Router = inject(Router);
  private readonly _userManagementService: IUserManagementService = inject(
    UserManagementService
  );
  private readonly _messageTransporterService: MessageTransporterService =
    inject(MessageTransporterService);

  constructor() {
    this._activatedRoute.data.subscribe((pageDataFromResolver) => {
      this.pageData = pageDataFromResolver['data'] as ILoginPage;
    });
  }

  public setEmail($event: string): void {
    this.loginData.email = $event;
  }

  public setPassword($event: string): void {
    this.loginData.password = $event;
  }

  public submitLogin(): void {
    this._userManagementService
      .login(this.loginData)
      .subscribe((userLoginDetailsResponse: IPublicUserDetailsResponse) => {
        const status: UserLoginEnum =
          userLoginDetailsResponse.publicUserQueryResponseStatus;

        if (status === UserLoginEnum.LOGIN_SUCCESSFUL) {
          this._messageTransporterService?.httpMessageInject.set(
            UserLoginEnum.LOGIN_SUCCESSFUL
          );

          // Just give the 'Welcome back!' toast message a second to show.
          setTimeout(() => {
            this._userManagementService?.saveLoggedInUser(
              userLoginDetailsResponse.publicUserDetails
            );
            this._userManagementService?.setLoggedInUser(
              userLoginDetailsResponse.publicUserDetails
            );
            this._router.navigate(['/home']);
          }, 1500);
        }

        if (status === UserLoginEnum.NO_SUCH_USER) {
          this._messageTransporterService.httpMessageInject.set(
            UserLoginEnum.NO_SUCH_USER
          );
          this.emailBad = true;
        }
        if (status === UserLoginEnum.INCORRECT_PASSWORD) {
          this._messageTransporterService.httpMessageInject.set(
            UserLoginEnum.INCORRECT_PASSWORD
          );
          this.passwordBad = true;
        }
      });
  }
}
