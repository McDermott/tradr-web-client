import {
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
  ResolveFn,
} from '@angular/router';
export interface ILoginPage {
  pageStrings: {
    siteName: string;
    siteSlogan: string;
    yourEmail: string;
    emailWrong: string;
    emailPlaceholder: string;
    yourPassword: string;
    passwordPlaceholder: string;
    passwordWrong: string;
    logIn: string;
    dontHaveAnAccount: string;
  };
}

export const loginPageResolver: ResolveFn<Partial<ILoginPage>> = (
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot
) => {
  return {
    pageStrings: {
      siteName: 'TRADR',
      siteSlogan: 'Your Digital Market Stall!',
      yourEmail: 'Your Email',
      emailPlaceholder: 'i.e. your.name@gmail.com',
      emailWrong: 'Your Email is not valid',
      yourPassword: 'Your Password',
      passwordWrong: 'Your password is wrong.',
      passwordPlaceholder: 'password',
      logIn: 'Log in!',
      dontHaveAnAccount: 'Create account.',
    },
  };
};
