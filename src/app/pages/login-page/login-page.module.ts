import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginPageComponent } from './login-page.component';
import { ReuseableModule } from 'src/app/components/reuseable/reuseable.module';
import { RouterLinkWithHref } from '@angular/router';

@NgModule({
  declarations: [LoginPageComponent],
  imports: [CommonModule, ReuseableModule, RouterLinkWithHref],
})
export class LoginPageModule {}
