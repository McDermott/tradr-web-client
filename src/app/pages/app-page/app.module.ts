import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { RoutingModule } from 'src/app/router/routing.module';
import { CommonModule } from '@angular/common';
import { ReuseableModule } from 'src/app/components/reuseable/reuseable.module';
import { LandingPageModule } from '../landing-page/landing-page.module';
import { RouterModule } from '@angular/router';
import { LoginPageModule } from '../login-page/login-page.module';
import { HomePageModule } from '../home-page/home-page.module';
import { SearchPageModule } from '../search-page/search-page.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    RoutingModule,
    RouterModule,
    CommonModule,
    LandingPageModule,
    LoginPageModule,
    HomePageModule,
    SearchPageModule,
    ReuseableModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
