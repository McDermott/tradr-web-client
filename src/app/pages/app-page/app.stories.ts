import { of } from 'rxjs';
import type { Meta, StoryObj } from '@storybook/angular';
import { ActivatedRoute } from '@angular/router';
import { moduleMetadata } from '@storybook/angular';
import { AppComponent } from '../app-page/app.component';

import { HeaderComponent } from 'src/app/components/reuseable/header/header.component';

const meta: Meta<AppComponent> = {
  title: 'App Component',
  component: AppComponent,
  excludeStories: /.*Data$/,
  decorators: [
    moduleMetadata({
      declarations: [HeaderComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({}),
          },
        },
      ],
    }),
  ],
  tags: ['autodocs'],
  render: (args: AppComponent) => ({
    props: {
      ...args,
    },
  }),
};

export default meta;

type Story = StoryObj<AppComponent>;

export const Default: Story = {
  args: {},
};
