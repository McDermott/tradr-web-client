export const baseDomain = 'http://localhost:8080/api/';

export const httpEndpoints: IHttpEndpoints = {
  // User endpoints.
  AUTH: baseDomain + 'auth',
  REGISTER: baseDomain + 'register',
  LOGIN: baseDomain + 'login',
  DELETE_USER: baseDomain + 'delete-user',
  UPDATE_USER: baseDomain + 'update-user',

  // Search endpoints.
  SEARCH: baseDomain + 'search/',

  // Store endpoints.
  CREATE_STORE: baseDomain + 'create-store',
  DELETE_STORE: baseDomain + 'delete-store',
  GET_STORE: baseDomain + 'get-store',
  GET_STORES_LIST: baseDomain + 'get-stores-list',
  UPDATE_STORE: baseDomain + 'update-store',

  // Item endpoints.
  DELETE_ITEM: baseDomain + 'delete-item',
  INSERT_ITEM: baseDomain + 'insert-item',

  // Admin endpoints.
  GET_REPORTED_STORES: baseDomain + 'get-reported-stores',
  MODERATE_STORE: baseDomain + 'moderate-store',
  REPORT_STORE: baseDomain + 'report-store',

  // Resource endpoints.
  STORE_IMAGE: baseDomain + 'store-images',
  USER_IMAGE: baseDomain + 'user-image',
  STORE_ITEM_IMAGE: baseDomain + 'store-item-image',
  CRAFT_TAGS: baseDomain + 'craft-tags',
  THEME_TAGS: baseDomain + 'theme-tags',
};

export interface IHttpEndpoints {
  AUTH: string;
  REGISTER: string;
  LOGIN: string;
  DELETE_USER: string;
  UPDATE_USER: string;
  SEARCH: string;
  CREATE_STORE: string;
  DELETE_STORE: string;
  GET_STORE: string;
  GET_STORES_LIST: string;
  UPDATE_STORE: string;
  DELETE_ITEM: string;
  INSERT_ITEM: string;
  GET_REPORTED_STORES: string;
  MODERATE_STORE: string;
  REPORT_STORE: string;
  STORE_IMAGE: string;
  USER_IMAGE: string;
  STORE_ITEM_IMAGE: string;
  CRAFT_TAGS: string;
  THEME_TAGS: string;
}
