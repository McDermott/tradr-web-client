export interface IUserInsert {
  userName: string;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  dob: string;
  avatar: string;
}
