export enum UserRegistrationResponseEnum {
  REGISTRATION_FAILED = 'REGISTRATION_FAILED',
  USER_EXISTS = 'USER_EXISTS',
  REGISTRATION_SUCCESSFUL = 'REGISTRATION_SUCCESSFUL',
}

export interface IUserRegistrationResponse {
  userRegistrationQueryStatus: UserRegistrationResponseEnum;
  userRegistrationFailureConditions: string[] | null;
}
