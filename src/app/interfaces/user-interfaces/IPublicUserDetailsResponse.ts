import { IPublicUserInterface } from './IPublicUserInterface';

export enum UserLoginEnum {
  NO_SUCH_USER = 'NO_SUCH_USER',
  INCORRECT_PASSWORD = 'INCORRECT_PASSWORD',
  LOGIN_SUCCESSFUL = 'LOGIN_SUCCESSFUL',
}

export interface IPublicUserDetailsResponse {
  publicUserQueryResponseStatus: UserLoginEnum;
  publicUserDetails: IPublicUserInterface;
}
