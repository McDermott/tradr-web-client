export enum UserUpdateInsertEnum {
  USER_NOT_AUTHORISED = 'USER_NOT_AUTHORISED',
  USER_UPDATE_FAILED_PROFANITY = 'USER_UPDATE_FAILED_PROFANITY',
  USER_UPDATED = 'USER_UPDATED',
  USER_UPDATE_FAILED = 'USER_UPDATE_FAILED',
}

export interface IUserUpdateInsert {
  authKey: string;
  userName: string;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  avatar: string;
}
