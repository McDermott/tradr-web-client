export interface IUserDetailsForAssociatedStore {
  userName: string;
  firstName: string;
  lastName: string;
  isSuspended: boolean;
  isDisabled: boolean;
}
