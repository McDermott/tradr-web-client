export interface IPublicUserInterface {
  userName: string;
  firstName: string;
  lastName: string;
  email: string;
  dob: string;
  registrationDate: string;
  authKey: string;
  uuid: string;
  isSuspended: boolean;
  isSuspendedDuration: string;
  isSuspendedReason: string;
  suspensionCount: number;
  isDisabled: boolean;
  isDisabledReason: string;
  isAdmin: boolean;
}
