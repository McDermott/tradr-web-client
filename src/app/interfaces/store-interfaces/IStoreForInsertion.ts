export interface IStoreForInsertion {
  authKey: string;
  storeTitle: string;
  storeDescription: string;
  isPrivate: boolean;
  storeTheme: string;
  craftTags: string[];
  addressLine1?: string;
  addressLine2?: string;
  addressLine3?: string;
  storePhone?: string;
  postcode?: string;
  storeAvatar: string;
  storeBanner: string;
}
