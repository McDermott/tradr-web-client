import { IStoreItem } from './IStoreItem';
import { IUserDetailsForAssociatedStore } from '../user-interfaces/IUserDetailsForAssociatedStore';

export interface IPublicStoreDetails {
  storeTitle: string;
  storeDescription: string;
  canMessage: boolean;
  isPrivate: boolean;
  isSuspended: boolean;
  isSuspendedDuration: string;
  isSuspendedReason: string;
  isDisabled: boolean;
  isDisabledReason: string;
  suspensionCount: number;
  reportedByUsers: string[];
  storeTheme: string;
  storeItems: IStoreItem[];
  craftTags: string[];
  addressLine1: string;
  addressLine2: string;
  addressLine3: string;
  storePhone: string;
  postcode: string;
  publicStoreId: string;
  associatedUser: IUserDetailsForAssociatedStore;
}
