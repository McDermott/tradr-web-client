import { IStoreSummary } from './IStoreSummary';

export enum StoreSummaryEnum {
  STORE_LIST_EMPTY = 'STORE_LIST_EMPTY',
  STORE_LIST_POPULATED = 'STORE_LIST_POPULATED',
}

export interface IStoreSummaryResponse {
  storeSummaryQueryStatus: StoreSummaryEnum;
  stores: IStoreSummary[] | null;
}
