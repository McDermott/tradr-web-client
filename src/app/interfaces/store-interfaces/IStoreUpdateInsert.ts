export enum StoreUpdateInsertEnum {
  STORE_UPDATE_FAILED = 'STORE_UPDATE_FAILED',
  STORE_UPDATED = 'STORE_UPDATED',
}

export interface IStoreUpdateInsert {
  authKey: string;
  storeDescription: string;
  storeTheme: string;
  isPrivate: boolean;
  addressLine1: string;
  addressLine2: string;
  addressLine3: string;
  storePhone: string;
  postcode: string;
  storeAvatar: string;
  storeBanner: string;
}
