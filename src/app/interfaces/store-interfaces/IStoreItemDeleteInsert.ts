export enum StoreItemDeleteEnum {
  ITEM_DELETED = 'ITEM_DELETED',
  ITEM_DELETION_FAILED = 'ITEM_DELETION_FAILED',
}

export interface IStoreItemDeleteInsert {
  authKey: string;
  storeItemPublicId: string;
}
