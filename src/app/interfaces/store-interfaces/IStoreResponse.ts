import { IPublicStoreDetails } from './IPublicStoreDetails';

export enum StoreResponseEnum {
  STORE_INSERTED = 'STORE_INSERTED',
  STORE_POPULATED = 'STORE_POPULATED',
  STORE_CREATION_FAILED_PROFANITY = 'STORE_POPULATED',
  STORE_CREATION_FAILED_STORE_EXISTS = 'STORE_POPULATED',
  INVALID_UUID = 'INVALID_UUID',
  STORE_INSERTION_FAILED = 'STORE_INSERTION_FAILED',
  STORE_EMPTY = 'STORE_EMPTY',
}

export interface IStoreResponse {
  storeQueryResponseStatus: StoreResponseEnum;
  store: IPublicStoreDetails | null;
}
