export interface IStoreItem {
  storeItemName: string;
  // This will always be null when returned from the server.
  // To get the storeItemImage we use the storeItemPublicId as the filename to the endpoint.
  // To insert an item we use this.
  storeItemImage: string | null;
  storeItemDescription: string;
  storeItemPrice: string;
  storeItemPublicId: string;
}
