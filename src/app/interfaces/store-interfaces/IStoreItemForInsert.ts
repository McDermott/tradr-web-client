import { IStoreItem } from './IStoreItem';

export enum StoreItemInsertEnum {
  ITEM_INSERTED = 'ITEM_INSERTED',
  ITEM_INSERTION_FAILED = 'ITEM_INSERTION_FAILED',
  ITEM_INSERTION_FAILED_PROFANITY = 'ITEM_INSERTION_FAILED_PROFANITY',
}

export interface IStoreItemForInsert {
  authKey: string;
  parentUUID: string;
  storeItem: IStoreItem;
}
