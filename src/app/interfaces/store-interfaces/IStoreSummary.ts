import { IUserDetailsForAssociatedStore } from '../user-interfaces/IUserDetailsForAssociatedStore';

export interface IStoreSummary {
  storeTitle: string;
  storeDescription: string;
  storeTheme: string;
  ownUUID: string | null;
  publicStoreId: string;
  suspensionCount: number;
  reports: number;
  associatedUser: IUserDetailsForAssociatedStore;
}
