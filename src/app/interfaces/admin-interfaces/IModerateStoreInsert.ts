export enum ModerateStoreInsertEnum {
  ADMIN_NOT_AUTHORISED = 'ADMIN_NOT_AUTHORISED',
  STORE_SUSPENSION_SUCCESSFUL = 'STORE_SUSPENSION_SUCCESSFUL',
  MODERATION_ACTION_FAILED = 'MODERATION_ACTION_FAILED',
  STORE_DISABLED_SUCCESSFUL = 'STORE_DISABLED_SUCCESSFUL',
}

export interface IModerateStoreInsert {
  authKey: string;
  storePublicId: string;
  // 0 = Suspend, 1 = Disable
  moderationType: number;
  moderationReason: string;
}
