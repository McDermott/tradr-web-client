export enum ReportStoreInsertEnum {
  USER_NOT_AUTHORISED = 'USER_NOT_AUTHORISED',
  USER_REPORT_UNSUCCESSFUL = 'USER_REPORT_UNSUCCESSFUL',
  USER_ALREADY_REPORTED = 'USER_ALREADY_REPORTED',
  USER_REPORT_SUCCESSFUL = 'USER_REPORT_SUCCESSFUL',
}

export interface IReportStoreInsert {
  authKey: string;
  reportingUUID: string;
  storePublicId: string;
}
