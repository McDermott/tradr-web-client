export enum SearchParamsInsertEnum {
  SEARCH_FAILED_NO_PARAMS = 'SEARCH_FAILED_NO_PARAMS',
  STORE_LIST_EMPTY = 'STORE_LIST_EMPTY',
  STORE_LIST_POPULATED = 'STORE_LIST_POPULATED',
}

export interface ISearchParamsInsert {
  [key: string]: any;
  textSearch?: string;
  postcode?: string;
  taglist?: Array<{ [key: string]: string }>;
  limit?: string;
}
