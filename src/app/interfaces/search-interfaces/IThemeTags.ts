export interface IThemeTags {
  [key: string]: any;
  default: string;
  dark: string;
  sandalwood: string;
  earth: string;
  meadow: string;
  forest: string;
  sky: string;
  ocean: string;
  cupcake: string;
  scarlet: string;
}
