import { MessageTransporterService } from 'src/app/services/message-transporter/message-transporter.service';
import { Component, effect, inject } from '@angular/core';

@Component({
  selector: 'tradr-tag-pill-bar',
  templateUrl: './tag-pill-bar.component.html',
  styleUrls: ['./tag-pill-bar.component.scss'],
})
export class TagPillBarComponent {
  public tags: Array<{ [key: string]: string }> = [];
  public tagsLengthError = false;

  private readonly _messageTransporterService: MessageTransporterService =
    inject(MessageTransporterService);

  constructor() {
    effect(() => {
      this.tags = this._messageTransporterService.searchPillsInject();
    });
  }

  public removeTag(tagToRemove: number): void {
    this.tagsLengthError = false;
    this.tags.splice(tagToRemove, 1);
    this._messageTransporterService.searchPillsInject.set(this.tags);
  }
}
