import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TagPillBarComponent } from './tag-pill-bar.component';

describe('TagPillBarComponent', () => {
  let component: TagPillBarComponent;
  let fixture: ComponentFixture<TagPillBarComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TagPillBarComponent]
    });
    fixture = TestBed.createComponent(TagPillBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
