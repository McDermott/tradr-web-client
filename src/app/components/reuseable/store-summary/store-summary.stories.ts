import type { Meta, StoryObj } from '@storybook/angular';
import { ActivatedRoute, RouterLinkWithHref } from '@angular/router';
import { componentWrapperDecorator, moduleMetadata } from '@storybook/angular';
import { AppComponent } from 'src/app/pages/app-page/app.component';
import { of } from 'rxjs';
import { StoreSummaryComponent } from './store-summary.component';
import { ButtonComponent } from '../button/button.component';

const meta: Meta<StoreSummaryComponent> = {
  title: 'Store Summary Component',
  component: StoreSummaryComponent,
  excludeStories: /.*Data$/,
  parameters: {},

  decorators: [
    componentWrapperDecorator(AppComponent),
    moduleMetadata({
      imports: [RouterLinkWithHref],
      declarations: [AppComponent, ButtonComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({}),
          },
        },
      ],
    }),
  ],

  tags: ['autodocs'],
  render: (args: StoreSummaryComponent) => ({
    props: {
      ...args,
    },
  }),
};

export default meta;

type Story = StoryObj<StoreSummaryComponent>;

export const Default: Story = {
  args: {
    storeSummary: {
      storeTitle: 'Test Store',
      storeDescription:
        'This is a test store. This is a description of the store. This is a test store. This is a description of the store. This is a test store. This is a description of the store.',
      storeTheme: 'default',
      ownUUID: null,
      publicStoreId: '@test-store-1234',
      suspensionCount: 0,
      reports: 0,
      associatedUser: {
        userName: '@test-user-1234',
        firstName: 'John',
        lastName: 'Smith',
        isSuspended: false,
        isDisabled: false,
      },
    },
  },
};

export const Sandalwood: Story = {
  args: {
    storeSummary: {
      storeTitle: 'Test Store',
      storeDescription:
        'This is a test store. This is a description of the store.',
      storeTheme: 'sandalwood',
      ownUUID: null,
      publicStoreId: '@test-store-1234',
      suspensionCount: 0,
      reports: 0,
      associatedUser: {
        userName: '@test-user-1234',
        firstName: 'John',
        lastName: 'Smith',
        isSuspended: false,
        isDisabled: false,
      },
    },
  },
};

export const Scarlet: Story = {
  args: {
    storeSummary: {
      storeTitle: 'Test Store',
      storeDescription:
        'This is a test store. This is a description of the store.',
      storeTheme: 'scarlet',
      ownUUID: null,
      publicStoreId: '@test-store-1234',
      suspensionCount: 0,
      reports: 0,
      associatedUser: {
        userName: '@test-user-1234',
        firstName: 'John',
        lastName: 'Smith',
        isSuspended: false,
        isDisabled: false,
      },
    },
  },
};

export const Sky: Story = {
  args: {
    storeSummary: {
      storeTitle: 'Test Store',
      storeDescription:
        'This is a test store. This is a description of the store.',
      storeTheme: 'sky',
      ownUUID: null,
      publicStoreId: '@test-store-1234',
      suspensionCount: 0,
      reports: 0,
      associatedUser: {
        userName: '@test-user-1234',
        firstName: 'John',
        lastName: 'Smith',
        isSuspended: false,
        isDisabled: false,
      },
    },
  },
};
