import { HttpErrorResponse } from '@angular/common/http';
import { storeThemes } from './../../../constants/store-themes';
import { Component, HostBinding, Input, inject } from '@angular/core';
import { catchError, of } from 'rxjs';
import { IStoreSummary } from 'src/app/interfaces/store-interfaces/IStoreSummary';
import {
  IResourceManagementService,
  ResourceManagementService,
} from 'src/app/services/resource-management-service/resource-management.service';

@Component({
  selector: 'tradr-store-summary',
  templateUrl: './store-summary.component.html',
  styleUrls: ['./store-summary.component.scss'],
})
export class StoreSummaryComponent {
  @Input() storeSummary?: IStoreSummary;

  private readonly _resouceManagementService: IResourceManagementService =
    inject(ResourceManagementService);

  @HostBinding('style.--theme-color') storeTheme = 'white';

  public storeAvatarValid = false;
  public storeBannerValid = false;
  public storeBannerURLString?: string;
  public storeAvatarURLString?: string;

  constructor() {}

  ngOnInit(): void {
    this.storeTheme =
      storeThemes[this.storeSummary?.storeTheme ?? 'white'] ?? 'white';

    if (this.storeSummary?.publicStoreId) {
      this._resouceManagementService
        ?.getStoreImageString('banner', this.storeSummary?.publicStoreId)
        .subscribe((bannerImageUrlString: string) => {
          if (bannerImageUrlString !== '') {
            this.storeBannerURLString = bannerImageUrlString;
          }
        });
      this._resouceManagementService
        ?.getStoreImageString('avatar', this.storeSummary?.publicStoreId)
        .subscribe((storeAvatarImageUrlString: string) => {
          if (storeAvatarImageUrlString !== '') {
            this.storeAvatarURLString = storeAvatarImageUrlString;
          }
        });
    }
  }
}
