import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreSummaryComponent } from './store-summary.component';

describe('StoreSummaryComponent', () => {
  let component: StoreSummaryComponent;
  let fixture: ComponentFixture<StoreSummaryComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [StoreSummaryComponent]
    });
    fixture = TestBed.createComponent(StoreSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
