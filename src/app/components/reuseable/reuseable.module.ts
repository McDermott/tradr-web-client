import { NgModule } from '@angular/core';
import { ButtonComponent } from './button/button.component';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { InputComponent } from './input/input.component';
import { SelectComponent } from './select/select.component';
import { ToastComponent } from './toast/toast.component';
import { ToggleComponent } from './toggle/toggle.component';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { RouterLinkWithHref } from '@angular/router';
import { StoreSummaryComponent } from './store-summary/store-summary.component';
import { SearchComponent } from './search/search.component';
import { TagPillBarComponent } from './tag-pill-bar/tag-pill-bar.component';

@NgModule({
  declarations: [
    ButtonComponent,
    FileUploadComponent,
    InputComponent,
    SelectComponent,
    ToastComponent,
    ToggleComponent,
    HeaderComponent,
    StoreSummaryComponent,
    SearchComponent,
    TagPillBarComponent,
  ],
  imports: [CommonModule, RouterLinkWithHref],
  exports: [
    ButtonComponent,
    FileUploadComponent,
    InputComponent,
    SelectComponent,
    ToastComponent,
    ToggleComponent,
    HeaderComponent,
    SearchComponent,
    StoreSummaryComponent,
    TagPillBarComponent,
  ],
  providers: [],
})
export class ReuseableModule {}
