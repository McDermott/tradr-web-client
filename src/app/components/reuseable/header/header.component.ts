import {
  IUserManagementService,
  UserManagementService,
} from 'src/app/services/user-management-service/user-management.service';
import { Component, effect, inject } from '@angular/core';
import { IPublicUserInterface } from 'src/app/interfaces/user-interfaces/IPublicUserInterface';
import { Router } from '@angular/router';

@Component({
  selector: 'tradr-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  public loggedInUser?: IPublicUserInterface;

  private readonly _userManagementService: IUserManagementService = inject(
    UserManagementService
  );

  private readonly _router = inject(Router);

  constructor() {
    effect(() => {
      this.loggedInUser = this._userManagementService.loggedInUser();
    });
  }

  ngOnInit(): void {}

  public logOut(): void {
    this._userManagementService.logOut();
  }

  public goHome(): void {
    this._router?.navigate(['home']);
  }
}
