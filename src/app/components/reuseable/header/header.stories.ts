import type { Meta, StoryObj } from '@storybook/angular';
import { ActivatedRoute, RouterLinkWithHref } from '@angular/router';
import { componentWrapperDecorator, moduleMetadata } from '@storybook/angular';

import { HeaderComponent } from './header.component';
import { AppComponent } from 'src/app/pages/app-page/app.component';
import { of } from 'rxjs';

const meta: Meta<HeaderComponent> = {
  title: 'Header Component',
  component: HeaderComponent,
  excludeStories: /.*Data$/,
  parameters: {},
  decorators: [
    componentWrapperDecorator(AppComponent),
    moduleMetadata({
      imports: [RouterLinkWithHref],
      declarations: [AppComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({}),
          },
        },
      ],
    }),
  ],
  tags: ['autodocs'],
  render: (args: HeaderComponent) => ({
    props: {
      ...args,
    },
  }),
};

export default meta;

type Story = StoryObj<HeaderComponent>;

export const Default: Story = {
  args: {
    loggedInUser: {
      userName: 'test',
      firstName: 'test',
      lastName: 'test',
      email: 'test',
      dob: 'test',
      registrationDate: 'test',
      authKey: 'test',
      uuid: 'test',
      isSuspended: false,
      isSuspendedDuration: '',
      isSuspendedReason: '',
      suspensionCount: 0,
      isDisabled: false,
      isDisabledReason: '',
      isAdmin: false,
    },
  },
};
