// import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
// import { HeaderComponent } from './header.component';
// import { UserService } from '../../_services/user-service/user.service';
// import { ILoggedInUser } from '../../../../../../libs/api-interfaces/src/lib/user.interface';
// import { of } from 'rxjs';

// describe('HeaderComponent', () => {
//   let component: HeaderComponent;
//   let fixture: ComponentFixture<HeaderComponent>;
//   let userService: UserService;

//   beforeEach(
//     waitForAsync(() => {
//       TestBed.configureTestingModule({
//         declarations: [HeaderComponent],
//         providers: [UserService],
//       }).compileComponents();
//     })
//   );

//   beforeEach(() => {
//     fixture = TestBed.createComponent(HeaderComponent);
//     component = fixture.componentInstance;
//     userService = TestBed.inject(UserService);
//     spyOn(userService.loggedInUser, 'subscribe').and.returnValue(
//       of({ name: 'John', role: 'admin' } as ILoggedInUser)
//     );
//     fixture.detectChanges();
//   });

//   it('should create', () => {
//     expect(component).toBeTruthy();
//   });

//   it('should set loggedInUser correctly on init', () => {
//     expect(component.loggedInUser).toEqual({
//       name: 'John',
//       role: 'admin',
//     } as ILoggedInUser);
//   });

//   it('should call logOut on button click', async () => {
//     spyOn(userService, 'logOut').and.callThrough();
//     const button = fixture.nativeElement.querySelector('button');
//     button.click();
//     fixture.detectChanges();
//     await fixture.whenStable();
//     expect(userService.logOut).toHaveBeenCalled();
//   });
// });
