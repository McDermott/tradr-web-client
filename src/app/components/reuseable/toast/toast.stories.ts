import type { Meta, StoryObj } from '@storybook/angular';
import { action } from '@storybook/addon-actions';
import { ToastComponent } from './toast.component';

export const actionsData = {
  output: action('output'),
};

const meta: Meta<ToastComponent> = {
  title: 'Toast Component',
  component: ToastComponent,
  excludeStories: /.*Data$/,
  tags: ['autodocs'],
  render: (args: ToastComponent) => ({
    props: {
      ...args,
    },
  }),
};

export default meta;

type Story = StoryObj<ToastComponent>;

export const Default: Story = {
  args: {},
};
