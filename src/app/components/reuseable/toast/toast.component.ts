import { GlobalEnumAmalgam } from './../../../services/message-transporter/message-transporter.service';
import { MessageTransporterService } from '../../../services/message-transporter/message-transporter.service';
import { Component, effect, inject } from '@angular/core';
import { ModerateStoreInsertEnum } from 'src/app/interfaces/admin-interfaces/IModerateStoreInsert';
import { ReportStoreInsertEnum } from 'src/app/interfaces/admin-interfaces/IReportStoreInsert';
import { SearchParamsInsertEnum } from 'src/app/interfaces/search-interfaces/ISearchParamsInsert';
import { StoreItemDeleteEnum } from 'src/app/interfaces/store-interfaces/IStoreItemDeleteInsert';
import { StoreItemInsertEnum } from 'src/app/interfaces/store-interfaces/IStoreItemForInsert';
import { StoreResponseEnum } from 'src/app/interfaces/store-interfaces/IStoreResponse';
import { StoreSummaryEnum } from 'src/app/interfaces/store-interfaces/IStoreSummaryResponse';
import { StoreUpdateInsertEnum } from 'src/app/interfaces/store-interfaces/IStoreUpdateInsert';
import { UserLoginEnum } from 'src/app/interfaces/user-interfaces/IPublicUserDetailsResponse';
import {
  UserAuthEnum,
  UserDeleteEnum,
} from 'src/app/interfaces/user-interfaces/IUserAuthKey';
import { UserRegistrationResponseEnum } from 'src/app/interfaces/user-interfaces/IUserRegistrationResponse';
import { UserUpdateInsertEnum } from 'src/app/interfaces/user-interfaces/IUserUpdateInsert';

export enum ToastGenericErrorsEnum {
  SERVER_FAILURE = 'SERVER_FAILURE',
  MAX_PILL_LIMIT = 'MAX_PILL_LIMIT',
}

@Component({
  selector: 'tradr-toast',
  templateUrl: './toast.component.html',
  styleUrls: ['./toast.component.scss'],
})
export class ToastComponent {
  public informationString?: string;
  public showMessage = false;
  public messageLengthDuration = 4100;
  public alertType?: 'fail' | 'success' | 'info' | 'none' = 'none';

  private readonly _messageTransporterService: MessageTransporterService =
    inject(MessageTransporterService);

  constructor() {
    effect(() => {
      const httpMessageType: GlobalEnumAmalgam =
        this._messageTransporterService.httpMessageInject();
      this.setInformationString(httpMessageType);
    });
  }

  private setInformationString(httpMessageType: GlobalEnumAmalgam): void {
    switch (httpMessageType) {
      case ToastGenericErrorsEnum.MAX_PILL_LIMIT:
        this.alertType = 'fail';
        this.informationString = 'You may only select a maximum of 3 pills.';
        this.showMessage = true;
        break;
      case ToastGenericErrorsEnum.SERVER_FAILURE:
        this.alertType = 'fail';
        this.informationString =
          'Something went wrong. Please try again later.';
        this.showMessage = true;
        break;

      case ModerateStoreInsertEnum.ADMIN_NOT_AUTHORISED:
        this.alertType = 'fail';
        this.informationString =
          'User does not have Administrative privileges.';
        this.showMessage = true;
        break;

      case ModerateStoreInsertEnum.MODERATION_ACTION_FAILED:
        this.alertType = 'fail';
        this.informationString = 'Moderation action failed.';
        this.showMessage = true;
        break;

      case ModerateStoreInsertEnum.STORE_DISABLED_SUCCESSFUL:
        this.alertType = 'info';
        this.informationString = 'Store successfully disabled.';
        this.showMessage = true;
        break;

      case ModerateStoreInsertEnum.STORE_SUSPENSION_SUCCESSFUL:
        this.alertType = 'info';
        this.informationString = 'Store successfully suspended.';
        this.showMessage = true;
        break;

      case ReportStoreInsertEnum.USER_ALREADY_REPORTED:
        this.alertType = 'fail';
        this.informationString = 'This store has already been reported.';
        this.showMessage = true;
        break;

      case ReportStoreInsertEnum.USER_NOT_AUTHORISED:
      case UserAuthEnum.USER_NOT_AUTHORISED:
      case UserUpdateInsertEnum.USER_NOT_AUTHORISED:
        this.alertType = 'fail';
        this.informationString = 'User not authorized.';
        this.showMessage = true;
        break;

      case ReportStoreInsertEnum.USER_REPORT_SUCCESSFUL:
        this.alertType = 'success';
        this.informationString = 'Successfully reported store for moderation.';
        this.showMessage = true;
        break;

      case ReportStoreInsertEnum.USER_REPORT_UNSUCCESSFUL:
        this.alertType = 'fail';
        this.informationString = 'Store report failure.';
        this.showMessage = true;
        break;

      case SearchParamsInsertEnum.SEARCH_FAILED_NO_PARAMS:
        this.alertType = 'info';
        this.informationString = 'Search failed: No criteria supplied.';
        this.showMessage = true;
        break;

      case SearchParamsInsertEnum.STORE_LIST_EMPTY:
      case StoreSummaryEnum.STORE_LIST_EMPTY:
        this.alertType = 'info';
        this.informationString = 'No store(s) available.';
        this.showMessage = true;
        break;

      case SearchParamsInsertEnum.STORE_LIST_POPULATED:
      case StoreSummaryEnum.STORE_LIST_POPULATED:
        this.alertType = 'none';
        this.informationString = '';
        this.showMessage = false;
        break;

      case StoreItemDeleteEnum.ITEM_DELETED:
        this.alertType = 'success';
        this.informationString = 'Item successfully removed.';
        this.showMessage = true;
        break;

      case StoreItemDeleteEnum.ITEM_DELETION_FAILED:
        this.alertType = 'fail';
        this.informationString = 'Item removal failed.';
        this.showMessage = true;
        break;

      case StoreItemInsertEnum.ITEM_INSERTED:
        this.alertType = 'success';
        this.informationString = 'Item successfully created!';
        this.showMessage = true;
        break;

      case StoreItemInsertEnum.ITEM_INSERTION_FAILED:
        this.alertType = 'fail';
        this.informationString = 'Failed to create item.';
        this.showMessage = true;
        break;

      case StoreResponseEnum.INVALID_UUID:
        this.alertType = 'fail';
        this.informationString = 'Store creation failed: Invalid User ID.';
        this.showMessage = true;
        break;

      case StoreResponseEnum.STORE_CREATION_FAILED_PROFANITY:
        this.alertType = 'fail';
        this.informationString = 'Store creation failed: Profanity.';
        this.showMessage = true;
        break;

      case StoreResponseEnum.STORE_CREATION_FAILED_STORE_EXISTS:
        this.alertType = 'fail';
        this.informationString = 'Store creation failed: Store exists.';
        this.showMessage = true;
        break;

      case StoreResponseEnum.STORE_EMPTY:
        this.alertType = 'info';
        this.informationString = 'Store(s) not found.';
        this.showMessage = true;
        break;

      case StoreResponseEnum.STORE_INSERTED:
        this.alertType = 'success';
        this.informationString = 'Store successfully created!';
        this.showMessage = true;
        break;

      case StoreResponseEnum.STORE_INSERTION_FAILED:
        this.alertType = 'fail';
        this.informationString = 'Store creation failed.';
        this.showMessage = true;
        break;

      case StoreResponseEnum.STORE_POPULATED:
        this.alertType = 'none';
        this.informationString = '';
        this.showMessage = false;
        break;

      case StoreUpdateInsertEnum.STORE_UPDATED:
        this.alertType = 'success';
        this.informationString = 'Store details successfully updated.';
        this.showMessage = true;
        break;

      case StoreUpdateInsertEnum.STORE_UPDATE_FAILED:
        this.alertType = 'fail';
        this.informationString = 'Store details update failed.';
        this.showMessage = true;
        break;

      case UserLoginEnum.INCORRECT_PASSWORD:
        this.alertType = 'fail';
        this.informationString = 'Login failed: Incorrect password.';
        this.showMessage = true;
        break;

      case UserLoginEnum.NO_SUCH_USER:
        this.alertType = 'fail';
        this.informationString = 'Login failed: No such user.';
        this.showMessage = true;
        break;

      case UserLoginEnum.LOGIN_SUCCESSFUL:
        this.alertType = 'info';
        this.informationString = 'Welcome back!';
        this.showMessage = true;
        break;

      case UserAuthEnum.USER_AUTHORISED:
        this.alertType = 'none';
        this.informationString = '';
        this.showMessage = false;
        break;

      case UserDeleteEnum.USER_DELETED:
        this.alertType = 'success';
        this.informationString = 'User successfully deleted.';
        this.showMessage = true;
        break;

      case UserDeleteEnum.USER_DELETION_FAILED:
        this.alertType = 'fail';
        this.informationString = 'User deletion failed.';
        this.showMessage = true;
        break;

      case UserRegistrationResponseEnum.REGISTRATION_FAILED:
        this.alertType = 'fail';
        this.informationString = 'User registration failed.';
        this.showMessage = true;
        break;

      case UserRegistrationResponseEnum.REGISTRATION_SUCCESSFUL:
        this.alertType = 'success';
        this.informationString = 'User registration successful.';
        this.showMessage = true;
        break;

      case UserRegistrationResponseEnum.USER_EXISTS:
        this.alertType = 'fail';
        this.informationString = 'User exists.';
        this.showMessage = true;
        break;

      case UserUpdateInsertEnum.USER_UPDATED:
        this.alertType = 'success';
        this.informationString = 'User successfully updated.';
        this.showMessage = true;
        break;

      case UserUpdateInsertEnum.USER_UPDATE_FAILED:
        this.alertType = 'fail';
        this.informationString = 'User update failed.';
        this.showMessage = true;
        break;

      case UserUpdateInsertEnum.USER_UPDATE_FAILED_PROFANITY:
        this.alertType = 'fail';
        this.informationString = 'User update failed: Profanity.';
        this.showMessage = true;
        break;

      default:
        this.alertType = 'none';
        this.informationString = '';
        this.showMessage = false;
        break;
    }
    setTimeout(() => {
      this.showMessage = false;
    }, this.messageLengthDuration);
  }
}
