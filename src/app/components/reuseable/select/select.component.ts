import {
  Component,
  EventEmitter,
  HostListener,
  Input,
  OnInit,
  Output,
  inject,
} from '@angular/core';
import { ICraftTags } from 'src/app/interfaces/search-interfaces/ICraftTags';
import { IThemeTags } from 'src/app/interfaces/search-interfaces/IThemeTags';
import {
  IResourceHttpAdaptorService,
  ResourceHttpAdaptorService,
} from 'src/app/services/resource-http-adaptor/resource-http-adaptor.service';

type IResourceTagUnion = ICraftTags | IThemeTags;

@Component({
  selector: 'tradr-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss'],
})
export class SelectComponent implements OnInit {
  @Input() readonly = false;
  @Input() value?: string;
  @Input() placeholder = '';
  @Input() hasError = false;
  @Input() label?: string;
  @Input() errorString?: string;
  @Input() resourceAcquisitionType: 'craftTags' | 'storeThemes' = 'craftTags';

  @Output() output: EventEmitter<{ [key: string]: string }> = new EventEmitter<{
    [key: string]: string;
  }>();

  public resourceList?: IResourceTagUnion;
  public selectIsOpen = false;
  public listOfItems: Array<any> = [];
  public browseableList: Array<string> = [];
  public validOptionsAvailable = false;
  public cachedPlaceholderString = '';
  public itemSelected = false;

  private readonly _resourceHttpAdaptorService: IResourceHttpAdaptorService =
    inject(ResourceHttpAdaptorService);

  @HostListener('document:click', ['$event.target'])
  documentClicked(): void {
    this.selectIsOpen = false;
    if (!this.validOptionsAvailable) {
      this.listOfItems = this.browseableList;
    }
    if (!this.itemSelected) {
      this.placeholder = this.cachedPlaceholderString;
    }
  }

  ngOnInit(): void {
    this.cachedPlaceholderString = this.placeholder;

    if (this.resourceAcquisitionType === 'craftTags') {
      this._resourceHttpAdaptorService
        ?.getCraftTags()
        .subscribe((crafTags: ICraftTags) => {
          this.resourceList = crafTags;
          this.handleResourceTags(crafTags as ICraftTags);
        });
    }

    if (this.resourceAcquisitionType === 'storeThemes') {
      this._resourceHttpAdaptorService
        ?.getThemeTags()
        .subscribe((themeTags: IThemeTags) => {
          this.resourceList = themeTags;
          this.handleResourceTags(themeTags as IThemeTags);
        });
    }
  }

  public handleResourceTags(resourceTags: IResourceTagUnion): void {
    Object.entries(resourceTags).forEach((resourceTag: [string, string]) => {
      this.listOfItems.push(resourceTag[1]);
    });
    this.browseableList = this.listOfItems;
  }

  public findValues($event: any): void {
    const inputValue = $event.target.value.toLowerCase();
    if (inputValue.length <= 3) {
      this.listOfItems = this.browseableList;
      this.selectIsOpen = true;
    } else {
      this.listOfItems = this.browseableList.filter((item) =>
        item.toLowerCase().includes(inputValue)
      );
      this.validOptionsAvailable = this.listOfItems.length > 0;
      this.hasError = this.listOfItems.length === 0;
    }
  }

  public openResourceSelect(): void {
    // Cheeky hack.
    this.value = '';
    this.placeholder = '';
    setTimeout(() => {
      this.selectIsOpen = true;
    }, 10);
  }

  public selectItem(item: string): void {
    let selectionHasError = true;

    for (const key in this.resourceList) {
      if (this.resourceList[key] === item) {
        this.output.emit({ [key]: this.resourceList[key] });
        this.placeholder = item;
        selectionHasError = false;
        this.itemSelected = true;
      } else {
        this.itemSelected = false;
      }
    }

    this.hasError = selectionHasError;
  }
}
