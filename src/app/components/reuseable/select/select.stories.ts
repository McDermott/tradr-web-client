import type { Meta, StoryObj } from '@storybook/angular';
import { action } from '@storybook/addon-actions';
import { SelectComponent } from './select.component';

export const actionsData = {
  output: action('output'),
};

const meta: Meta<SelectComponent> = {
  title: 'Select Component',
  component: SelectComponent,
  excludeStories: /.*Data$/,
  tags: ['autodocs'],
  parameters: {
    layout: 'centered',
    docs: {
      story: {
        inline: false,
        iframeHeight: 500,
      },
    },
  },
  render: (args: SelectComponent) => ({
    props: {
      ...args,
      Output: actionsData.output,
    },
  }),
};

export default meta;

type Story = StoryObj<SelectComponent>;

export const Default: Story = {
  args: {
    readonly: false,
    value: 'Select',
    placeholder: 'Select',
    hasError: false,
    label: 'Typeahead',
    errorString: 'Error',
    resourceAcquisitionType: 'craftTags',
  },
};
