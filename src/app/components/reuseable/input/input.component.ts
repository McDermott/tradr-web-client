import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'tradr-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
})
export class InputComponent {
  @Input() readonly = false;
  @Input() value?: string;
  @Input() placeholder? = '';
  @Input() inputType?: 'text' | 'password';
  @Input() hasError = false;
  @Input() label?: string;
  @Input() errorString?: string;

  @Output() output: EventEmitter<string> = new EventEmitter<string>();

  public emitInput(event: any): void {
    this.output.emit(event.target.value);
  }

  public setInputType(): string {
    return this.inputType ?? 'text';
  }
}
