import type { Meta, StoryObj } from '@storybook/angular';
import { action } from '@storybook/addon-actions';
import { InputComponent } from './input.component';

export const actionsData = {
  output: action('output'),
};

const meta: Meta<InputComponent> = {
  title: 'Text Input Component',
  component: InputComponent,
  excludeStories: /.*Data$/,
  tags: ['autodocs'],
  render: (args: InputComponent) => ({
    props: {
      ...args,
      output: actionsData.output,
    },
  }),
};

export default meta;

type Story = StoryObj<InputComponent>;

export const Default: Story = {
  args: {
    readonly: false,
    label: 'Test Input',
    value: 'Test value',
    placeholder: 'Test placeholder',
    inputType: 'text',
    hasError: false,
    errorString: 'Test input error',
  },
};

export const Password: Story = {
  args: {
    readonly: false,
    label: 'Test Input',
    value: '',
    placeholder: 'Test placeholder',
    inputType: 'password',
    hasError: false,
    errorString: 'Test input error',
  },
};

export const Readonly: Story = {
  args: {
    readonly: true,
    label: 'Test Input',
    value: 'Test Value',
    placeholder: '',
    inputType: 'text',
    hasError: false,
    errorString: 'Test input error',
  },
};

export const HasError: Story = {
  args: {
    readonly: false,
    label: 'Test Input',
    value: 'Test value',
    placeholder: 'Test placeholder',
    inputType: 'text',
    hasError: true,
    errorString: 'Test input error',
  },
};
