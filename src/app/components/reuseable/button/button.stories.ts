import type { Meta, StoryObj } from '@storybook/angular';
import { action } from '@storybook/addon-actions';
import { ButtonComponent } from './button.component';

export const actionsData = {
  output: action('output'),
};

const meta: Meta<ButtonComponent> = {
  title: 'Button Component',
  component: ButtonComponent,
  excludeStories: /.*Data$/,
  tags: ['autodocs'],
  render: (args: ButtonComponent) => ({
    props: {
      ...args,
      output: actionsData.output,
    },
  }),
};

export default meta;

type Story = StoryObj<ButtonComponent>;

export const Default: Story = {
  args: {
    label: 'Submit',
  },
};
