import {
  Component,
  EventEmitter,
  HostBinding,
  Input,
  Output,
} from '@angular/core';
import { storeThemes } from 'src/app/constants/store-themes';

@Component({
  selector: 'tradr-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
})
export class ButtonComponent {
  @Input() label? = 'Submit';
  @Input() theme?: string;
  @Input() isSearchButton = false;

  @HostBinding('style.--theme-color') storeTheme = '#ac4f1b';

  @Output() output: EventEmitter<any> = new EventEmitter();

  ngOnInit(): void {
    if (this.theme === 'white' || this.theme === 'default') {
      this.storeTheme = storeThemes['orange'] ?? '#ac4f1b';
    } else {
      this.storeTheme = storeThemes[this.theme ?? 'orange'] ?? 'white';
    }
  }
}
