import type { Meta, StoryObj } from '@storybook/angular';
import { action } from '@storybook/addon-actions';
import { FileUploadComponent } from './file-upload.component';

export const actionsData = {
  output: action('output'),
};

const meta: Meta<FileUploadComponent> = {
  title: 'File Upload Component',
  component: FileUploadComponent,
  excludeStories: /.*Data$/,
  tags: ['autodocs'],
  render: (args: FileUploadComponent) => ({
    props: {
      ...args,
      output: actionsData.output,
    },
  }),
};

export default meta;

type Story = StoryObj<FileUploadComponent>;

export const Default: Story = {
  args: {
    buttonText: 'Upload User/Store Avatar',
    fileType: 'avatar',
  },
};

export const ProductImage: Story = {
  args: {
    buttonText: 'Upload Product Image',
    fileType: 'product_image',
  },
};

export const StoreBanner: Story = {
  args: {
    buttonText: 'Upload Store Banner',
    fileType: 'banner',
  },
};
