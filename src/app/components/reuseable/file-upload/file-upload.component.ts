import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'tradr-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss'],
})
export class FileUploadComponent {
  isValidImageForUploaded = false;
  fileUploadDirty = false;
  fileSelected = false;

  @Input() buttonText = 'Upload';
  @Input() fileType: 'banner' | 'product_image' | 'avatar' = 'avatar';

  @Output() output: EventEmitter<string> = new EventEmitter<string>();

  public selectFile($event: any): void {
    let sizeIsValid = false;
    let dimensionsAreValid = false;
    let maxWidth = 0;
    let maxHeight = 0;
    let maxSize = 0;

    // Set the maximum dimensions and file size based on the file type
    if (this.fileType === 'banner') {
      maxWidth = 1920;
      maxHeight = 600;
      maxSize = 3000000;
    } else if (
      this.fileType === 'avatar' ||
      this.fileType === 'product_image'
    ) {
      maxWidth = 500;
      maxHeight = 500;
      maxSize = 1000000;
    }

    const uploadedImage: any = $event.target.files[0];

    sizeIsValid = uploadedImage.size <= maxSize;

    const fileReader = new FileReader();
    fileReader.onload = () => {
      const image: any = new Image();
      image.onload = () => {
        this.fileUploadDirty = true;
        dimensionsAreValid =
          image.width <= maxWidth && image.height <= maxHeight;
        const isValidImage = dimensionsAreValid && sizeIsValid;
        if (isValidImage) {
          const base64Image = fileReader.result as string;
          this.output.emit(base64Image);
        }
        this.fileUploadDirty = !isValidImage;
        this.fileSelected = isValidImage;
        this.isValidImageForUploaded = isValidImage;
      };
      image.src = fileReader.result as string;
    };
    fileReader.readAsDataURL(uploadedImage);
  }
}
