import type { Meta, StoryObj } from '@storybook/angular';
import { action } from '@storybook/addon-actions';
import { ToggleComponent } from './toggle.component';

export const actionsData = {
  output: action('output'),
};

const meta: Meta<ToggleComponent> = {
  title: 'Toggle Component',
  component: ToggleComponent,
  excludeStories: /.*Data$/,
  tags: ['autodocs'],
  render: (args: ToggleComponent) => ({
    props: {
      ...args,
      Output: actionsData.output,
    },
  }),
};

export default meta;

type Story = StoryObj<ToggleComponent>;

export const Default: Story = {
  args: {
    checked: false,
  },
};
