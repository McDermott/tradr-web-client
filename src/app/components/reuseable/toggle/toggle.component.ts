import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'tradr-toggle',
  templateUrl: './toggle.component.html',
  styleUrls: ['./toggle.component.scss'],
})
export class ToggleComponent {
  @Input() checked = false;
  @Output() output: EventEmitter<boolean> = new EventEmitter<boolean>();

  public toggleCheck(): void {
    this.checked = !this.checked;
    this.output.emit(this.checked);
  }
}
