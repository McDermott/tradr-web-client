import { MessageTransporterService } from 'src/app/services/message-transporter/message-transporter.service';
import { Component, effect, inject } from '@angular/core';
import {
  IResourceHttpAdaptorService,
  ResourceHttpAdaptorService,
} from 'src/app/services/resource-http-adaptor/resource-http-adaptor.service';
import { SearchManagementService } from 'src/app/services/search-management-service/search-management.service';
import { ToastGenericErrorsEnum } from '../toast/toast.component';
import { ISearchParamsInsert } from 'src/app/interfaces/search-interfaces/ISearchParamsInsert';

@Component({
  selector: 'tradr-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent {
  public isLocation = false;
  public selectedSearchTags: Array<{ [key: string]: string }> = [];
  public storeNameSearch?: string;
  public storePostcodeSearch?: string;

  private readonly _messageTransporterService: MessageTransporterService =
    inject(MessageTransporterService);
  private readonly _resourceHttpAdaptor: IResourceHttpAdaptorService = inject(
    ResourceHttpAdaptorService
  );
  private readonly _searchManagementService: SearchManagementService = inject(
    SearchManagementService
  );

  constructor() {
    effect(() => {
      this.selectedSearchTags =
        this._messageTransporterService.searchPillsInject();
    });
  }

  public handleSearchButtonClick(): void {
    const paramsToSend: ISearchParamsInsert = {
      textSearch: this.storeNameSearch ?? undefined,
      postcode: this.storePostcodeSearch ?? undefined,
      taglist:
        this.selectedSearchTags.length > 0
          ? this.selectedSearchTags
          : undefined,
      limit: '20',
    };

    this._searchManagementService.buildAndRouteSearchQuery(paramsToSend);
  }

  public handleStoreNameSearch($event: string): void {
    this.storeNameSearch = $event;
  }

  public handlePostcodeInput($event: string): void {
    this.storePostcodeSearch = $event;
  }

  public handleTagSelection(tag: any): void {
    this.selectedSearchTags.push(tag);

    const uniqueTags = this.selectedSearchTags.filter((obj, index, self) => {
      return (
        index ===
        self.findIndex((t) => JSON.stringify(t) === JSON.stringify(obj))
      );
    });

    if (uniqueTags.length === 4) {
      uniqueTags.pop();
      this._messageTransporterService.httpMessageInject.set(
        ToastGenericErrorsEnum.MAX_PILL_LIMIT
      );
    }

    this.selectedSearchTags = uniqueTags;

    this._messageTransporterService.searchPillsInject.set(
      this.selectedSearchTags
    );
  }

  public handleToggle(): void {
    this.isLocation = !this.isLocation;
    this.selectedSearchTags = [];
    this._messageTransporterService.searchPillsInject.set([]);
  }
}
