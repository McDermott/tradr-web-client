import type { Meta, StoryObj } from '@storybook/angular';
import { ActivatedRoute, RouterLinkWithHref } from '@angular/router';
import { componentWrapperDecorator, moduleMetadata } from '@storybook/angular';
import { AppComponent } from 'src/app/pages/app-page/app.component';
import { of } from 'rxjs';
import { SearchComponent } from './search.component';
import { ButtonComponent } from '../button/button.component';
import { ToggleComponent } from '../toggle/toggle.component';
import { InputComponent } from '../input/input.component';

const meta: Meta<SearchComponent> = {
  title: 'Search Component',
  component: SearchComponent,
  excludeStories: /.*Data$/,
  parameters: {},
  decorators: [
    componentWrapperDecorator(AppComponent),
    moduleMetadata({
      imports: [RouterLinkWithHref],
      declarations: [
        AppComponent,
        ButtonComponent,
        ToggleComponent,
        InputComponent,
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({}),
          },
        },
      ],
    }),
  ],
  tags: ['autodocs'],
  render: (args: SearchComponent) => ({
    props: {
      ...args,
    },
  }),
};

export default meta;

type Story = StoryObj<SearchComponent>;

export const Default: Story = {
  args: {},
};
