import { Injectable, inject } from '@angular/core';
import {
  IStoreHttpAdaptorService,
  StoreHttpAdaptorService,
} from '../store-http-adaptor/store-http-adaptor.service';
import { Observable } from 'rxjs';
import { IStoreSummaryResponse } from 'src/app/interfaces/store-interfaces/IStoreSummaryResponse';

@Injectable({
  providedIn: 'root',
})
export class StoreManagementService {
  private readonly _storeHttpAdaptor: IStoreHttpAdaptorService = inject(
    StoreHttpAdaptorService
  );

  constructor() {}

  public getMostRecentStoresForHomePageDisplay(): Observable<IStoreSummaryResponse> {
    return this._storeHttpAdaptor.getStoresList('9');
  }
}
