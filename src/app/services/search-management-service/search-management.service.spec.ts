import { TestBed } from '@angular/core/testing';

import { SearchManagementService } from './search-management.service';

describe('SearchManagementService', () => {
  let service: SearchManagementService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SearchManagementService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
