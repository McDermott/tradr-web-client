import { Injectable, inject } from '@angular/core';
import {
  ISearchHttpAdaptorService,
  SearchHttpAdaptorService,
} from '../search-http-adaptor/search-http-adaptor.service';
import { ISearchParamsInsert } from 'src/app/interfaces/search-interfaces/ISearchParamsInsert';
import { NavigationExtras, Router } from '@angular/router';

export interface ISearchManagementService {}

@Injectable({
  providedIn: 'root',
})
export class SearchManagementService implements ISearchManagementService {
  private readonly _searchHttpAdaptorService: ISearchHttpAdaptorService =
    inject(SearchHttpAdaptorService);
  private readonly _router: Router = inject(Router);

  public buildAndRouteSearchQuery(searchParams: ISearchParamsInsert): void {
    const searchTagListStringArray = searchParams.taglist?.map(
      (obj) => Object.keys(obj)[0]
    );
    const concatinatedSearchTagListString = searchTagListStringArray?.join(',');

    const newParams: { [key: string]: string | undefined } = {
      limit: searchParams.limit,
      postcode: searchParams.postcode,
      taglist: concatinatedSearchTagListString ?? undefined,
      textsearch: searchParams.textSearch,
    };

    // for (const searchQueryParameterKey in newParams) {
    //   console.log('searchQueryParameterKey', searchQueryParameterKey);
    //   if (searchParams[searchQueryParameterKey]) {
    //     searchQueryString =
    //       searchQueryString +
    //       `?${searchQueryParameterKey}=${newParams[searchQueryParameterKey]}`;
    //   }
    // }

    const navigationalExtras: NavigationExtras = {
      queryParams: newParams,
    };

    this._router.navigate(['/search'], navigationalExtras);
  }
}
