import { TestBed } from '@angular/core/testing';

import { ResourceHttpAdaptorService } from './resource-http-adaptor.service';

describe('ResourceHttpAdaptorService', () => {
  let service: ResourceHttpAdaptorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ResourceHttpAdaptorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
