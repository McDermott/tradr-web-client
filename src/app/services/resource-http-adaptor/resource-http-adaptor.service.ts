import { Injectable } from '@angular/core';
import { HttpBaseService } from '../http-base/http-base.service';
import { Observable } from 'rxjs';
import { ICraftTags } from 'src/app/interfaces/search-interfaces/ICraftTags';
import { IThemeTags } from 'src/app/interfaces/search-interfaces/IThemeTags';

export interface IResourceHttpAdaptorService {
  getStoreImageString: (
    imagetype: 'banner' | 'avatar',
    param: string
  ) => Observable<string>;
  getStoreItemImageString: (param: string) => Observable<string>;
  getUserImageString: (param: string) => Observable<string>;
  getCraftTags: () => Observable<ICraftTags>;
  getThemeTags: () => Observable<IThemeTags>;
}

@Injectable({
  providedIn: 'root',
})
export class ResourceHttpAdaptorService implements IResourceHttpAdaptorService {
  constructor(private _httpService: HttpBaseService) {}

  public getStoreImageString(
    imagetype: 'banner' | 'avatar',
    param: string
  ): Observable<string> {
    return this._httpService.getStoreImageString(imagetype, param);
  }

  public getStoreItemImageString(param: string): Observable<string> {
    return this._httpService.getStoreItemImageString(param);
  }

  public getUserImageString(param: string): Observable<string> {
    return this._httpService.getUserImageString(param);
  }

  public getCraftTags(): Observable<ICraftTags> {
    return this._httpService.getCraftTags();
  }

  public getThemeTags(): Observable<IThemeTags> {
    return this._httpService.getThemeTags();
  }
}
