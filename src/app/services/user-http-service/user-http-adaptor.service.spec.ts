import { TestBed } from '@angular/core/testing';

import { UserHttpAdaptorService } from './user-http-adaptor.service';

describe('UserHttpAdaptorService', () => {
  let service: UserHttpAdaptorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UserHttpAdaptorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
