import { Injectable, inject } from '@angular/core';
import {
  HttpBaseService,
  IHttpBaseService,
} from '../http-base/http-base.service';
import {
  IUserAuthKey,
  UserAuthEnum,
  UserDeleteEnum,
} from 'src/app/interfaces/user-interfaces/IUserAuthKey';
import { Observable } from 'rxjs';
import { IUserLoginDetailsInsert } from 'src/app/interfaces/user-interfaces/IUserLoginDetailsInsert';
import { IPublicUserDetailsResponse } from 'src/app/interfaces/user-interfaces/IPublicUserDetailsResponse';
import {
  IUserUpdateInsert,
  UserUpdateInsertEnum,
} from 'src/app/interfaces/user-interfaces/IUserUpdateInsert';
import { IUserRegistrationResponse } from 'src/app/interfaces/user-interfaces/IUserRegistrationResponse';
import { IUserInsert } from 'src/app/interfaces/user-interfaces/IUserInsert';
import {
  IReportStoreInsert,
  ReportStoreInsertEnum,
} from 'src/app/interfaces/admin-interfaces/IReportStoreInsert';

export interface IUserHttpAdaptorService {
  postAuth: (params: IUserAuthKey) => Observable<UserAuthEnum>;
  postDeleteUser: (params: IUserAuthKey) => Observable<UserDeleteEnum>;
  postLogin: (
    params: IUserLoginDetailsInsert
  ) => Observable<IPublicUserDetailsResponse>;
  postUpdateUser: (
    params: IUserUpdateInsert
  ) => Observable<UserUpdateInsertEnum>;
  postUserRegistration: (
    params: IUserInsert
  ) => Observable<IUserRegistrationResponse>;
  postReportStore: (
    params: IReportStoreInsert
  ) => Observable<ReportStoreInsertEnum>;
}

@Injectable({
  providedIn: 'root',
})
export class UserHttpAdaptorService implements IUserHttpAdaptorService {
  private _httpBaseService: IHttpBaseService = inject(HttpBaseService);

  constructor() {}

  public postAuth(params: IUserAuthKey): Observable<UserAuthEnum> {
    return this._httpBaseService.postAuth(params);
  }

  public postDeleteUser(params: IUserAuthKey): Observable<UserDeleteEnum> {
    return this._httpBaseService.postDeleteUser(params);
  }

  public postLogin(
    params: IUserLoginDetailsInsert
  ): Observable<IPublicUserDetailsResponse> {
    return this._httpBaseService.postLogin(params);
  }

  public postUpdateUser(
    params: IUserUpdateInsert
  ): Observable<UserUpdateInsertEnum> {
    return this._httpBaseService.postUpdateUser(params);
  }

  public postUserRegistration(
    params: IUserInsert
  ): Observable<IUserRegistrationResponse> {
    return this._httpBaseService.postUserRegistration(params);
  }

  public postReportStore(
    params: IReportStoreInsert
  ): Observable<ReportStoreInsertEnum> {
    return this._httpBaseService.postReportStore(params);
  }
}
