import { Injectable, inject } from '@angular/core';
import {
  IResourceHttpAdaptorService,
  ResourceHttpAdaptorService,
} from '../resource-http-adaptor/resource-http-adaptor.service';
import { Observable } from 'rxjs';
import { ICraftTags } from 'src/app/interfaces/search-interfaces/ICraftTags';
import { IThemeTags } from 'src/app/interfaces/search-interfaces/IThemeTags';

export interface IResourceManagementService {
  getStoreImageString: (
    imagetype: 'banner' | 'avatar',
    param: string
  ) => Observable<string>;
  getStoreItemImageString: (param: string) => Observable<string>;
  getUserImageString: (param: string) => Observable<string>;
  getCraftTags: () => Observable<ICraftTags>;
  getThemeTags: () => Observable<IThemeTags>;
}

@Injectable({
  providedIn: 'root',
})
export class ResourceManagementService implements IResourceManagementService {
  private readonly _resourceHttpAdaptorService: IResourceHttpAdaptorService =
    inject(ResourceHttpAdaptorService);

  public getStoreImageString(
    imagetype: 'banner' | 'avatar',
    param: string
  ): Observable<string> {
    return this._resourceHttpAdaptorService.getStoreImageString(
      imagetype,
      param
    );
  }

  public getStoreItemImageString(param: string): Observable<string> {
    return this._resourceHttpAdaptorService.getStoreItemImageString(param);
  }

  public getUserImageString(param: string): Observable<string> {
    return this._resourceHttpAdaptorService.getUserImageString(param);
  }

  public getCraftTags(): Observable<ICraftTags> {
    return this._resourceHttpAdaptorService.getCraftTags();
  }

  public getThemeTags(): Observable<IThemeTags> {
    return this._resourceHttpAdaptorService.getThemeTags();
  }
}
