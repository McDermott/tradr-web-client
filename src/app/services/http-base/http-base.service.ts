import {
  IReportStoreInsert,
  ReportStoreInsertEnum,
} from '../../interfaces/admin-interfaces/IReportStoreInsert';
import { IUserInsert } from '../../interfaces/user-interfaces/IUserInsert';
import { Injectable, inject } from '@angular/core';
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
  HttpXhrBackend,
} from '@angular/common/http';
import {
  IUserAuthKey,
  StoreDeleteEnum,
  UserAuthEnum,
  UserDeleteEnum,
} from 'src/app/interfaces/user-interfaces/IUserAuthKey';
import { Observable, catchError, of } from 'rxjs';
import { httpEndpoints } from 'src/app/constants/httpendpoints';
import { ICraftTags } from 'src/app/interfaces/search-interfaces/ICraftTags';
import { IStoreForInsertion } from 'src/app/interfaces/store-interfaces/IStoreForInsertion';
import { IStoreResponse } from 'src/app/interfaces/store-interfaces/IStoreResponse';
import {
  IStoreItemDeleteInsert,
  StoreItemDeleteEnum,
} from 'src/app/interfaces/store-interfaces/IStoreItemDeleteInsert';
import {
  IStoreItemForInsert,
  StoreItemInsertEnum,
} from 'src/app/interfaces/store-interfaces/IStoreItemForInsert';
import { IUserLoginDetailsInsert } from 'src/app/interfaces/user-interfaces/IUserLoginDetailsInsert';
import { IPublicUserDetailsResponse } from 'src/app/interfaces/user-interfaces/IPublicUserDetailsResponse';
import {
  IModerateStoreInsert,
  ModerateStoreInsertEnum,
} from 'src/app/interfaces/admin-interfaces/IModerateStoreInsert';
import { IUserRegistrationResponse } from 'src/app/interfaces/user-interfaces/IUserRegistrationResponse';
import { ISearchParamsInsert } from 'src/app/interfaces/search-interfaces/ISearchParamsInsert';
import {
  IStoreUpdateInsert,
  StoreUpdateInsertEnum,
} from 'src/app/interfaces/store-interfaces/IStoreUpdateInsert';
import {
  IUserUpdateInsert,
  UserUpdateInsertEnum,
} from 'src/app/interfaces/user-interfaces/IUserUpdateInsert';
import { IStoreSummaryResponse } from 'src/app/interfaces/store-interfaces/IStoreSummaryResponse';
import { IThemeTags } from 'src/app/interfaces/search-interfaces/IThemeTags';
import { MessageTransporterService } from '../message-transporter/message-transporter.service';
import { ToastGenericErrorsEnum } from 'src/app/components/reuseable/toast/toast.component';

export interface IHttpBaseService {
  postAuth: (params: IUserAuthKey) => Observable<UserAuthEnum>;
  getCraftTags: () => Observable<ICraftTags>;
  getThemeTags: () => Observable<IThemeTags>;
  postCreateStore: (params: IStoreForInsertion) => Observable<IStoreResponse>;
  postDeleteItem: (
    params: IStoreItemDeleteInsert
  ) => Observable<StoreItemDeleteEnum>;
  postDeleteStore: (params: IUserAuthKey) => Observable<StoreDeleteEnum>;
  postDeleteUser: (params: IUserAuthKey) => Observable<UserDeleteEnum>;
  getReportedStores: (
    params: IUserAuthKey,
    limit?: string
  ) => Observable<IStoreSummaryResponse>;
  getStore: (storeId: string) => Observable<IStoreResponse>;
  getStoresList: (limit?: string) => Observable<IStoreSummaryResponse>;
  postInsertItem: (
    params: IStoreItemForInsert
  ) => Observable<StoreItemInsertEnum>;
  postLogin: (
    params: IUserLoginDetailsInsert
  ) => Observable<IPublicUserDetailsResponse>;
  postModerateStore: (
    params: IModerateStoreInsert
  ) => Observable<ModerateStoreInsertEnum>;
  postUserRegistration: (
    params: IUserInsert
  ) => Observable<IUserRegistrationResponse>;
  postReportStore: (
    params: IReportStoreInsert
  ) => Observable<ReportStoreInsertEnum>;
  postSearch: (paramsString: string) => Observable<IStoreSummaryResponse>;
  postUpdateStore: (
    params: IStoreUpdateInsert
  ) => Observable<StoreUpdateInsertEnum>;
  postUpdateUser: (
    params: IUserUpdateInsert
  ) => Observable<UserUpdateInsertEnum>;
  getStoreImageString: (
    imagetype: 'banner' | 'avatar',
    param: string
  ) => Observable<string>;
  getStoreItemImageString: (param: string) => Observable<string>;
  getUserImageString: (param: string) => Observable<string>;
}

@Injectable({
  providedIn: 'root',
})
export class HttpBaseService implements IHttpBaseService {
  private headers = new HttpHeaders().set('Content-Type', 'application/json');

  private readonly _httpClient: HttpClient = new HttpClient(
    new HttpXhrBackend({
      build: () => new XMLHttpRequest(),
    })
  );

  constructor(
    private readonly _messageTransporterService: MessageTransporterService
  ) {}

  private catchHttpError() {
    this._messageTransporterService.httpMessageInject.set(
      ToastGenericErrorsEnum.SERVER_FAILURE
    );
  }

  public postAuth(params: IUserAuthKey): Observable<UserAuthEnum> {
    return this._httpClient
      .post<UserAuthEnum>(httpEndpoints.AUTH, params, {
        ...this.headers,
      })
      .pipe(
        catchError((error: HttpErrorResponse): Observable<UserAuthEnum> => {
          this.catchHttpError();
          throw error;
        })
      );
  }

  public getCraftTags(): Observable<ICraftTags> {
    return this._httpClient.get<ICraftTags>(httpEndpoints.CRAFT_TAGS).pipe(
      catchError((error: HttpErrorResponse): Observable<ICraftTags> => {
        this.catchHttpError();
        throw error;
      })
    );
  }

  public getThemeTags(): Observable<IThemeTags> {
    return this._httpClient.get<IThemeTags>(httpEndpoints.THEME_TAGS).pipe(
      catchError((error: HttpErrorResponse): Observable<IThemeTags> => {
        this.catchHttpError();
        throw error;
      })
    );
  }

  public postCreateStore(
    params: IStoreForInsertion
  ): Observable<IStoreResponse> {
    return this._httpClient
      .post<IStoreResponse>(httpEndpoints.CREATE_STORE, params, {
        ...this.headers,
      })
      .pipe(
        catchError((error: HttpErrorResponse): Observable<IStoreResponse> => {
          this.catchHttpError();
          throw error;
        })
      );
  }

  public postDeleteItem(
    params: IStoreItemDeleteInsert
  ): Observable<StoreItemDeleteEnum> {
    return this._httpClient
      .post<StoreItemDeleteEnum>(httpEndpoints.DELETE_ITEM, params, {
        ...this.headers,
      })
      .pipe(
        catchError(
          (error: HttpErrorResponse): Observable<StoreItemDeleteEnum> => {
            this.catchHttpError();
            throw error;
          }
        )
      );
  }

  public postDeleteStore(params: IUserAuthKey): Observable<StoreDeleteEnum> {
    return this._httpClient
      .post<StoreDeleteEnum>(httpEndpoints.DELETE_STORE, params, {
        ...this.headers,
      })
      .pipe(
        catchError((error: HttpErrorResponse): Observable<StoreDeleteEnum> => {
          this.catchHttpError();
          throw error;
        })
      );
  }

  public postDeleteUser(params: IUserAuthKey): Observable<UserDeleteEnum> {
    return this._httpClient
      .post<UserDeleteEnum>(httpEndpoints.DELETE_USER, params, {
        ...this.headers,
      })
      .pipe(
        catchError((error: HttpErrorResponse): Observable<UserDeleteEnum> => {
          this.catchHttpError();
          throw error;
        })
      );
  }

  public getReportedStores(
    params: IUserAuthKey,
    limit?: string
  ): Observable<IStoreSummaryResponse> {
    const endpointString: string = limit
      ? httpEndpoints.GET_REPORTED_STORES
      : httpEndpoints.GET_REPORTED_STORES + `/&limit = ${limit}`;
    return this._httpClient
      .post<IStoreSummaryResponse>(endpointString, params, {
        ...this.headers,
      })
      .pipe(
        catchError(
          (error: HttpErrorResponse): Observable<IStoreSummaryResponse> => {
            this.catchHttpError();
            throw error;
          }
        )
      );
  }

  public getStore(storeId: string): Observable<IStoreResponse> {
    if (!storeId) {
      throw new Error('No STOREID provided!');
    }
    const endpointString: string = httpEndpoints.GET_STORE + `/${storeId}`;
    return this._httpClient.get<IStoreResponse>(endpointString).pipe(
      catchError((error: HttpErrorResponse): Observable<IStoreResponse> => {
        this.catchHttpError();
        throw error;
      })
    );
  }

  public getStoresList(limit?: string): Observable<IStoreSummaryResponse> {
    console.log('limit', limit);
    const endpointString: string = limit
      ? httpEndpoints.GET_STORES_LIST + `?limit=${limit}`
      : httpEndpoints.GET_STORES_LIST;
    console.log('endpointString', endpointString);
    return this._httpClient.get<IStoreSummaryResponse>(endpointString).pipe(
      catchError(
        (error: HttpErrorResponse): Observable<IStoreSummaryResponse> => {
          this.catchHttpError();
          throw error;
        }
      )
    );
  }

  public postInsertItem(
    params: IStoreItemForInsert
  ): Observable<StoreItemInsertEnum> {
    return this._httpClient
      .post<StoreItemInsertEnum>(httpEndpoints.INSERT_ITEM, params, {
        ...this.headers,
      })
      .pipe(
        catchError(
          (error: HttpErrorResponse): Observable<StoreItemInsertEnum> => {
            this.catchHttpError();
            throw error;
          }
        )
      );
  }

  public postLogin(
    params: IUserLoginDetailsInsert
  ): Observable<IPublicUserDetailsResponse> {
    return this._httpClient
      .post<IPublicUserDetailsResponse>(httpEndpoints.LOGIN, params, {
        ...this.headers,
      })
      .pipe(
        catchError(
          (
            error: HttpErrorResponse
          ): Observable<IPublicUserDetailsResponse> => {
            this.catchHttpError();
            throw error;
          }
        )
      );
  }

  public postModerateStore(
    params: IModerateStoreInsert
  ): Observable<ModerateStoreInsertEnum> {
    return this._httpClient
      .post<ModerateStoreInsertEnum>(httpEndpoints.MODERATE_STORE, params, {
        ...this.headers,
      })
      .pipe(
        catchError(
          (error: HttpErrorResponse): Observable<ModerateStoreInsertEnum> => {
            this.catchHttpError();
            throw error;
          }
        )
      );
  }

  public postUserRegistration(
    params: IUserInsert
  ): Observable<IUserRegistrationResponse> {
    return this._httpClient
      .post<IUserRegistrationResponse>(httpEndpoints.REGISTER, params, {
        ...this.headers,
      })
      .pipe(
        catchError(
          (error: HttpErrorResponse): Observable<IUserRegistrationResponse> => {
            this.catchHttpError();
            throw error;
          }
        )
      );
  }

  public postReportStore(
    params: IReportStoreInsert
  ): Observable<ReportStoreInsertEnum> {
    return this._httpClient
      .post<ReportStoreInsertEnum>(httpEndpoints.REPORT_STORE, params, {
        ...this.headers,
      })
      .pipe(
        catchError(
          (error: HttpErrorResponse): Observable<ReportStoreInsertEnum> => {
            this.catchHttpError();
            throw error;
          }
        )
      );
  }

  public postSearch(paramsString: string): Observable<IStoreSummaryResponse> {
    let searchQuery = httpEndpoints.SEARCH + paramsString;

    return this._httpClient
      .post<IStoreSummaryResponse>(searchQuery, {
        ...this.headers,
      })
      .pipe(
        catchError(
          (error: HttpErrorResponse): Observable<IStoreSummaryResponse> => {
            this.catchHttpError();
            throw error;
          }
        )
      );
  }

  public postUpdateStore(
    params: IStoreUpdateInsert
  ): Observable<StoreUpdateInsertEnum> {
    return this._httpClient
      .post<StoreUpdateInsertEnum>(httpEndpoints.UPDATE_STORE, params, {
        ...this.headers,
      })
      .pipe(
        catchError(
          (error: HttpErrorResponse): Observable<StoreUpdateInsertEnum> => {
            this.catchHttpError();
            throw error;
          }
        )
      );
  }

  public postUpdateUser(
    params: IUserUpdateInsert
  ): Observable<UserUpdateInsertEnum> {
    return this._httpClient
      .post<UserUpdateInsertEnum>(httpEndpoints.UPDATE_USER, params, {
        ...this.headers,
      })
      .pipe(
        catchError(
          (error: HttpErrorResponse): Observable<UserUpdateInsertEnum> => {
            this.catchHttpError();
            throw error;
          }
        )
      );
  }

  public getStoreImageString(
    imagetype: 'banner' | 'avatar',
    param: string
  ): Observable<string> {
    return this._httpClient
      .get<string>(`${httpEndpoints.STORE_IMAGE}/${imagetype}/${param}.jpg`)
      .pipe(
        catchError((error: HttpErrorResponse) => {
          if (error.status != 200) {
            return of('');
          }
          return of(error.url ?? '');
        })
      );
  }

  public getStoreItemImageString(param: string): Observable<string> {
    return this._httpClient
      .get<string>(`${httpEndpoints.STORE_ITEM_IMAGE}/${param}.jpg`)
      .pipe(
        catchError((error: HttpErrorResponse) => {
          if (error.status != 200) {
            return of('');
          }
          return of(error.url ?? '');
        })
      );
  }

  public getUserImageString(param: string): Observable<string> {
    return this._httpClient
      .get<string>(`${httpEndpoints.USER_IMAGE}/${param}.jpg`)
      .pipe(
        catchError((error: HttpErrorResponse) => {
          if (error.status != 200) {
            return of('');
          }
          return of(error.url ?? '');
        })
      );
  }
}
