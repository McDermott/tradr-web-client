import { TestBed } from '@angular/core/testing';

import { MessageTransporterService } from './message-transporter.service';

describe('MessageTransporterService', () => {
  let service: MessageTransporterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MessageTransporterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
