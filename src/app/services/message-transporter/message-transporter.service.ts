import { Injectable, WritableSignal, signal } from '@angular/core';
import { ModerateStoreInsertEnum } from 'src/app/interfaces/admin-interfaces/IModerateStoreInsert';
import { ReportStoreInsertEnum } from 'src/app/interfaces/admin-interfaces/IReportStoreInsert';
import { SearchParamsInsertEnum } from 'src/app/interfaces/search-interfaces/ISearchParamsInsert';
import { StoreItemDeleteEnum } from 'src/app/interfaces/store-interfaces/IStoreItemDeleteInsert';
import { StoreItemInsertEnum } from 'src/app/interfaces/store-interfaces/IStoreItemForInsert';
import { StoreResponseEnum } from 'src/app/interfaces/store-interfaces/IStoreResponse';
import { StoreSummaryEnum } from 'src/app/interfaces/store-interfaces/IStoreSummaryResponse';
import { StoreUpdateInsertEnum } from 'src/app/interfaces/store-interfaces/IStoreUpdateInsert';
import { UserLoginEnum } from 'src/app/interfaces/user-interfaces/IPublicUserDetailsResponse';
import {
  UserAuthEnum,
  UserDeleteEnum,
} from 'src/app/interfaces/user-interfaces/IUserAuthKey';
import { UserRegistrationResponseEnum } from 'src/app/interfaces/user-interfaces/IUserRegistrationResponse';
import { UserUpdateInsertEnum } from 'src/app/interfaces/user-interfaces/IUserUpdateInsert';

export type GlobalEnumAmalgam =
  | string
  | UserLoginEnum
  | UserAuthEnum
  | ModerateStoreInsertEnum
  | ReportStoreInsertEnum
  | SearchParamsInsertEnum
  | StoreItemDeleteEnum
  | StoreItemInsertEnum
  | StoreResponseEnum
  | StoreSummaryEnum
  | StoreUpdateInsertEnum
  | UserLoginEnum
  | UserAuthEnum
  | StoreItemDeleteEnum
  | UserDeleteEnum
  | UserRegistrationResponseEnum
  | UserUpdateInsertEnum;

@Injectable({
  providedIn: 'root',
})
export class MessageTransporterService {
  private DEFAULT_VALUE = 'DEFAULT_VALUE';
  private DEFAULT_OBJECT = {
    DEFAULT_VALUE: 'DEFAULT_VALUE',
  };

  public httpMessageInject: WritableSignal<GlobalEnumAmalgam> = signal(
    this.DEFAULT_VALUE
  );

  public searchPillsInject: WritableSignal<Array<{ [key: string]: string }>> =
    signal([]);

  constructor() {}
}
