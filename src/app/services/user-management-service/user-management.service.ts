import { IPublicUserDetailsResponse } from './../../interfaces/user-interfaces/IPublicUserDetailsResponse';
import { IUserLoginDetailsInsert } from './../../interfaces/user-interfaces/IUserLoginDetailsInsert';
import { Injectable, WritableSignal, inject, signal } from '@angular/core';
import { IPublicUserInterface } from 'src/app/interfaces/user-interfaces/IPublicUserInterface';
import {
  IUserHttpAdaptorService,
  UserHttpAdaptorService,
} from '../user-http-service/user-http-adaptor.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

export interface IUserManagementService {
  loggedInUser: WritableSignal<IPublicUserInterface | undefined>;
  login: (
    userLoginDetails: IUserLoginDetailsInsert
  ) => Observable<IPublicUserDetailsResponse>;
  saveLoggedInUser: (userToSave: IPublicUserInterface) => void;
  getSavedUser: () => IPublicUserInterface | undefined;
  setLoggedInUser: (userToSet: IPublicUserInterface) => void;
  logOut: () => void;
  doUserCheck: () => void;
}

@Injectable({
  providedIn: 'root',
})
export class UserManagementService implements IUserManagementService {
  public loggedInUser: WritableSignal<IPublicUserInterface | undefined> =
    signal<IPublicUserInterface | undefined>(undefined);

  private readonly _userHttpAdaptor: IUserHttpAdaptorService = inject(
    UserHttpAdaptorService
  );
  private readonly _router: Router = inject(Router);

  public login(
    userLoginDetails: IUserLoginDetailsInsert
  ): Observable<IPublicUserDetailsResponse> {
    return this._userHttpAdaptor.postLogin(userLoginDetails);
  }

  public saveLoggedInUser(userToSave: IPublicUserInterface): void {
    if (window.localStorage.getItem('TRADR_LOGIN_DATA')) {
      localStorage.removeItem('TRADR_LOGIN_DATA');
    }
    window.localStorage.setItem('TRADR_LOGIN_DATA', JSON.stringify(userToSave));
  }

  public getSavedUser(): IPublicUserInterface | undefined {
    const userData = localStorage.getItem('TRADR_LOGIN_DATA');
    if (userData) {
      return JSON.parse(userData);
    }
    return undefined;
  }

  public setLoggedInUser(userToSet?: IPublicUserInterface) {
    this.loggedInUser.set(userToSet);
  }

  public logOut(): void {
    this.loggedInUser.set(undefined);
    localStorage.removeItem('TRADR_LOGIN_DATA');
    this._router?.navigate(['login']);
  }

  public async doUserCheck(): Promise<void> {
    if (this.getSavedUser()) {
      await this.setLoggedInUser(this.getSavedUser());
      // console.log('womo?');
      this._router?.navigate(['home']);
    } else {
      this._router?.navigate(['login']);
    }
  }
}
