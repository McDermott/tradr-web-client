import { Injectable } from '@angular/core';
import { HttpBaseService } from '../http-base/http-base.service';
import { IStoreResponse } from 'src/app/interfaces/store-interfaces/IStoreResponse';
import { IStoreForInsertion } from 'src/app/interfaces/store-interfaces/IStoreForInsertion';
import { Observable } from 'rxjs';
import {
  IStoreItemDeleteInsert,
  StoreItemDeleteEnum,
} from 'src/app/interfaces/store-interfaces/IStoreItemDeleteInsert';
import { IStoreSummaryResponse } from 'src/app/interfaces/store-interfaces/IStoreSummaryResponse';
import {
  IStoreItemForInsert,
  StoreItemInsertEnum,
} from 'src/app/interfaces/store-interfaces/IStoreItemForInsert';
import {
  IUserAuthKey,
  StoreDeleteEnum,
} from 'src/app/interfaces/user-interfaces/IUserAuthKey';

export interface IStoreHttpAdaptorService {
  postCreateStore: (params: IStoreForInsertion) => Observable<IStoreResponse>;
  postDeleteItem: (
    params: IStoreItemDeleteInsert
  ) => Observable<StoreItemDeleteEnum>;
  postDeleteStore: (params: IUserAuthKey) => Observable<StoreDeleteEnum>;
  getStore: (params: string) => Observable<IStoreResponse>;
  getStoresList: (limit?: string) => Observable<IStoreSummaryResponse>;
  postInsertItem: (
    params: IStoreItemForInsert
  ) => Observable<StoreItemInsertEnum>;
}

@Injectable({
  providedIn: 'root',
})
export class StoreHttpAdaptorService implements IStoreHttpAdaptorService {
  constructor(private _httpService: HttpBaseService) {}

  public postCreateStore(
    params: IStoreForInsertion
  ): Observable<IStoreResponse> {
    return this._httpService.postCreateStore(params);
  }

  public postDeleteItem(
    params: IStoreItemDeleteInsert
  ): Observable<StoreItemDeleteEnum> {
    return this._httpService.postDeleteItem(params);
  }

  public postDeleteStore(params: IUserAuthKey): Observable<StoreDeleteEnum> {
    return this._httpService.postDeleteStore(params);
  }

  public getStore(params: string): Observable<IStoreResponse> {
    return this._httpService.getStore(params);
  }

  public getStoresList(limit?: string): Observable<IStoreSummaryResponse> {
    return this._httpService.getStoresList(limit);
  }

  public postInsertItem(
    params: IStoreItemForInsert
  ): Observable<StoreItemInsertEnum> {
    return this._httpService.postInsertItem(params);
  }
}
