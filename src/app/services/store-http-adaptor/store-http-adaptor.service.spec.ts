import { TestBed } from '@angular/core/testing';

import { StoreHttpAdaptorService } from './store-http-adaptor.service';

describe('StoreHttpAdaptorService', () => {
  let service: StoreHttpAdaptorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StoreHttpAdaptorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
