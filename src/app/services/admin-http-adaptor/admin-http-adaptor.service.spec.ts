import { TestBed } from '@angular/core/testing';

import { AdminHttpAdaptorService } from './admin-http-adaptor.service';

describe('AdminHttpAdaptorService', () => {
  let service: AdminHttpAdaptorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AdminHttpAdaptorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
