import { Injectable } from '@angular/core';
import { HttpBaseService } from '../http-base/http-base.service';
import {
  IModerateStoreInsert,
  ModerateStoreInsertEnum,
} from 'src/app/interfaces/admin-interfaces/IModerateStoreInsert';
import { Observable } from 'rxjs';
import { IUserAuthKey } from 'src/app/interfaces/user-interfaces/IUserAuthKey';
import { IStoreSummaryResponse } from 'src/app/interfaces/store-interfaces/IStoreSummaryResponse';

export interface IAdminHttpAdaptorService {
  postModerateStore: (
    params: IModerateStoreInsert
  ) => Observable<ModerateStoreInsertEnum>;
  getReportedStores: (
    params: IUserAuthKey,
    limit?: string
  ) => Observable<IStoreSummaryResponse>;
}

@Injectable({
  providedIn: 'root',
})
export class AdminHttpAdaptorService implements IAdminHttpAdaptorService {
  constructor(private _httpService: HttpBaseService) {}

  public postModerateStore(
    params: IModerateStoreInsert
  ): Observable<ModerateStoreInsertEnum> {
    return this._httpService.postModerateStore(params);
  }

  public getReportedStores(
    params: IUserAuthKey,
    limit?: string
  ): Observable<IStoreSummaryResponse> {
    return this._httpService.getReportedStores(params, limit);
  }
}
