import { IStoreSummaryResponse } from './../../interfaces/store-interfaces/IStoreSummaryResponse';
import { Injectable, inject } from '@angular/core';
import {
  HttpBaseService,
  IHttpBaseService,
} from '../http-base/http-base.service';
import { Observable } from 'rxjs';

export interface ISearchHttpAdaptorService {
  postSearch: (paramsString: string) => Observable<IStoreSummaryResponse>;
}

@Injectable({
  providedIn: 'root',
})
export class SearchHttpAdaptorService implements ISearchHttpAdaptorService {
  private readonly _httpService: IHttpBaseService = inject(HttpBaseService);

  public postSearch(paramsString: string): Observable<IStoreSummaryResponse> {
    return this._httpService.postSearch(paramsString);
  }
}
