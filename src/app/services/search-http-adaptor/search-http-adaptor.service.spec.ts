import { TestBed } from '@angular/core/testing';

import { SearchHttpAdaptorService } from './search-http-adaptor.service';

describe('SearchHttpAdaptorService', () => {
  let service: SearchHttpAdaptorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SearchHttpAdaptorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
